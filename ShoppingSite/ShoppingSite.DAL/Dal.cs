﻿using ShoppingSite.BusinessObjects;

namespace ShoppingSite.DAL
{
    public class Dal
    {
        ShoppingSiteDbContext db = new ShoppingSiteDbContext();

        public int AddUser(User user)
        {
            db.Users.Add(user);
            db.SaveChanges();
            return 1;
        }

        public int DeleteUser(int userid)
        {
            User user = db.Users.Find(userid);
            if (user != null)
            {
                user.IsActive=false;
                db.SaveChanges();
                return 1;
            }
            return 0;
        }

        public User GetUser(int id)
        {
            User user = db.Users.Find(id);
            if (user != null)
            {
                return user;
            }
            return null;
        }

        public Role GetRole(int id)
        {
            Role role = db.Roles.Find(id);
            return role;
        }

        public int AddProduct(Product prod) {
            db.Products.Add(prod);
            db.SaveChanges();
            return 1;
        }

        public int DeleteProduct(int productid)
        {
            Product prod = db.Products.Find(productid);
            if (prod != null)
            {
                db.Products.Remove(prod);
                db.SaveChanges();
                return 1;
            }
            return 0;
        }

        public int EditProduct(int productid,float price,int qty)
        {
            Product prod = db.Products.Find(productid);
            if (prod != null)
            {
                prod.Price=price;
                prod.Quantity=qty;
                db.SaveChanges();
                return 1;
            }
            return 0;
        }

        public Product GetProduct(int id)
        {
            Product prod = db.Products.Find(id);
            if (prod != null)
            {
                return prod;
            }
            return null;
        }

        public List<Product> GetProducts()
        {
            List<Product> products = new List<Product>();
            products = db.Products.ToList();
            return products;
        }

        public int PlaceOrder(Order ord) {
            db.Orders.Add(ord);
            db.SaveChanges();
            return 1;
        }
    }
}