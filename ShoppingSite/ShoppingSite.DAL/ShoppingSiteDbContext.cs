﻿using Microsoft.EntityFrameworkCore;
using ShoppingSite.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingSite.DAL
{
    public class ShoppingSiteDbContext:DbContext
    {
        public ShoppingSiteDbContext() { 
        }

        public ShoppingSiteDbContext(DbContextOptions<ShoppingSiteDbContext> options) : base(options) { }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                @"data source=LAPTOP-CP97IDC6;initial catalog=ShoppingSiteDb;integrated security=true;
TrustServerCertificate=True");
        }
    }
}
