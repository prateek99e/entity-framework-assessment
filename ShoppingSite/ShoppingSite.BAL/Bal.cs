﻿using ShoppingSite.BusinessObjects;
using ShoppingSite.DAL;

namespace ShoppingSite.BAL
{
    public class Bal
    {
        Dal dal = new Dal();
        public int AddUser(User user)
        {
            return dal.AddUser(user);

        }

        public Role GetRole(int roleid)
        {
            return dal.GetRole(roleid);

        }

        public int DeleteUser(int userid)
        {
            return dal.DeleteUser(userid);
        }

        public User GetUser(int userid)
        {
            return dal.GetUser(userid);
        }

        public int AddProduct(Product prod)
        {
            return dal.AddProduct(prod);

        }

        public int DeleteProduct(int productid)
        {
            return dal.DeleteProduct(productid);
        }

        public int EditProduct(int productid,float price, int qty)
        {
            return dal.EditProduct(productid:productid,price:price,qty:qty);
        }

        public List<Product> GetProducts() {
        return dal.GetProducts();
        }

        public Product GetProduct(int productid)
        {
            return dal.GetProduct(productid);
        }

        public int PlaceOrder(Order ord) {
            return dal.PlaceOrder(ord);
        }
    }
}